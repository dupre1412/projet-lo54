@echo off
set /p user="Entrez l'utilisateur de la base de donnee : "
mysql --host=localhost --user=%user% -p < db.sql && (
	echo La base de donnée est instalée
) || (
	echo L'installation de la base de donnee a échoué
)
pause