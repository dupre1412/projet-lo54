<%@ include file="/WEB-INF/views/util.jsp" %>
<head>
	<title>Sessions pour ${course.getCode_course()}</title>
</head>
<body>
	<div class="container">
	<h2>Liste des sessions disponibles pour le cours : ${course.getCode_course()} - ${course.getTitle()}</h2>
	</br>
	<table class="table">
		<tr>
			<th>Lieu</th>
			<th>Date de d�but</th>
			<th>Date de fin</th>
			<th>Statut</th>
			<th>Inscription</th>
		</tr>
	<c:forEach var="session" items="${sessions}">
		<tr>
				<td>${session.getLocation().getCity()}</td>
				<td>${session.getStartDate()}</td>
				<td>${session.getEndDate()}</td>
				<td>
					<c:if test="${session.getStatus()=='open' || session.getStatus()=='already'}">
						${session.getCurrentStudentNumber()}/${session.getMaxStudent()}
					</c:if>
					<c:if test="${session.getStatus()=='ongoing'}">
						<span class="text-warning">En cours</span>
					</c:if>
					<c:if test="${session.getStatus()=='ended'}">
						<span class="text-danger">Termin�</span>
					</c:if>
				</td>
				<td>
					<sec:authorize access="isAuthenticated()">
					<c:if test="${session.getStatus()=='open'}">
						<button class="btn btn-primary" onclick="confirmInscription(${session.getIdCourseSession()},'${session.getStartDate()}','${session.getEndDate()}','${session.getLocation().getCity()}')">S'inscrire</button>
					</c:if>
					<c:if test="${session.getStatus()=='already'}">
						<button class="btn btn-danger" onclick="removeInscription(${session.getIdCourseSession()})">Se d�sinscrire</button>
					</c:if>
					</sec:authorize>
				</td>
		</tr>
	</c:forEach>
	</table>
	
	</div>
</body>

<script type="text/javascript">
	function reloadPage(){
	    location.reload(true);
	}

	function confirmInscription(idSession,startDate,endDate,location){
		if(confirm("Confirmez-vous votre inscription � la session du cours ${course.getCode_course()} : \nSession du "+startDate+" au "+ endDate +" � "+location+"\nLes informations sur votre profil seront reprises et un mail de validation vous sera envoy�")){
			$.post( "${pageContext.request.contextPath}/sessions", { idSession: idSession, code: '${course.getCode_course()}', startDate: startDate, endDate: endDate, location: location} );
			reloadPage();
		}
	}

	function removeInscription(idSession){
		if(confirm("Voulez-vous vous d�sinscrire de cette session ?")){
			$.post( "${pageContext.request.contextPath}/noSessions", { idSession: idSession, code: '${course.getCode_course()}'} );
			reloadPage();
		}
	}
</script>
</html>

