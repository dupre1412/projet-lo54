<%@ include file="/WEB-INF/views/util.jsp" %>
<head>
	<title>Home</title>
</head>
<body>
	<div class="container">
	<h2>Liste des cours disponibles</h2>
	</br>
	<form method="POST" action="<c:url value="/home"/>">
		<div class="row">
			<div class="col-sm-3">
				<p>Titre contient : </p>
				<input class="form-control" type="text" name="title" value="${title}"/>
			</div>
			<div class="col-sm-3">
				<p>A une session le : </p>
				<input class="form-control" type="date" name="session_date" value="${date}"/>
			</div>
			<div class="col-sm-3">
				<p>Peut se situer � : </p>
				<select class="form-control form-control-sm" name="location" >
					<option value="0" ></option>
					<c:forEach var="loc" items="${locations}">
						<c:if test="${loc.getId_location()==location}">
							<option value="${loc.getId_location()}" selected>${loc.getCity()}</option>
						</c:if>
						<c:if test="${loc.getId_location()!=location}">
							<option value="${loc.getId_location()}" >${loc.getCity()}</option>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="col-sm-3">
				<p></p>
				<button type="submit" class="btn btn-primary float-right">Filtrer</button>
			</div>
		</div>
	</form>
	<table class="table">
		<tr>
			<th>Code</th>
			<th>Nom formation</th>
			<th>Prochaine session</th>
			<th>Sessions</th>
		</tr>
	<c:forEach var="course" items="${courses}">
		<tr>
				<td>${course.course.getCode_course()}</td>
				<td>${course.course.getTitle()}</td>
				<td>${course.startDate}</td>
				<td><a class="btn btn-primary" href="<c:url value="/sessions"/>?course=${course.course.getCode_course()}">-></a></td>
		</tr>
	</c:forEach>
	</table>
	
	</div>
</body>
</html>

