<%@ include file="/WEB-INF/views/util.jsp" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title><spring:message code="user.createAccount" text="Cr�er un compte"/></title>
  </head>

  <body>

    <div class="container">

        <form:form method="POST" modelAttribute="userForm" class="form-signin">
            <h2 class="form-signin-heading"><spring:message code="user.createAccount" text="Cr�er un compte"/></h2>
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4><spring:message code="user.username" text="Identifiant"/><span class="text-danger">*</span></h4>
                	<spring:message code="user.username" var="placeholder_username"/>
                    <form:input type="text" path="username" class="form-control" placeholder="${placeholder_username}" autofocus="true"></form:input>
                    <form:errors path="username"></form:errors>
                </div>

                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <h4><spring:message code="user.password" text="Mot de passe"/><span class="text-danger">*</span></h4>
                    <spring:message code="user.password" var="placeholder_password"/>
                    <form:input type="password" path="password" class="form-control" placeholder="${placeholder_password}"></form:input>
                    <form:errors path="password"></form:errors>
                </div>

                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <h4><spring:message code="user.confirmPassword" text="Confirmer le mot de passe"/><span class="text-danger">*</span></h4>
                    <form:input type="password" path="passwordConfirm" class="form-control"
                                placeholder=""></form:input>
                    <form:errors path="passwordConfirm"></form:errors>
                </div>
            
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4><spring:message code="user.mail" text="Email"/><span class="text-danger">*</span></h4>
                    <spring:message code="user.mail" var="placeholder_mail"/>
                    <form:input type="email" path="email" class="form-control"
                                placeholder="${placeholder_mail}"></form:input>
                    <form:errors path="email"></form:errors>
                </div>
            
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4><spring:message code="user.firstname" text="Pr�nom"/><span class="text-danger">*</span></h4>
                    <spring:message code="user.firstname" var="placeholder_firstname"/>
                    <form:input type="text" path="firstname" class="form-control"
                                placeholder="${placeholder_firstname}"></form:input>
                    <form:errors path="firstname"></form:errors>
                </div>
            
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4><spring:message code="user.lastname" text="Nom"/><span class="text-danger">*</span></h4>
                    <spring:message code="user.lastname" var="placeholder_lastname"/>
                    <form:input type="text" path="lastname" class="form-control"
                                placeholder="${placeholder_lastname}"></form:input>
                    <form:errors path="lastname"></form:errors>
                </div>
            
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4><spring:message code="user.address" text="Adresse"/></h4>
                    <spring:message code="user.address" var="placeholder_address"/>
                    <form:input type="text" path="address" class="form-control"
                                placeholder="${placeholder_address}"></form:input>
                    <form:errors path="address"></form:errors>
                </div>
            
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4><spring:message code="user.phone" text="Num�ro de t�l�phone"/></h4>
                    <spring:message code="user.phone" var="placeholder_phone"/>
                    <form:input type="tel" path="phone" class="form-control"
                                placeholder="${placeholder_phone}"></form:input>
                    <form:errors path="phone"></form:errors>
                </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="button.confirm" text="Confirmer"/></button>
        </form:form>
        <p><span class="text-danger">*</span> : <spring:message code="user.fieldRequired" text="champs obligatoires"/></p>

    </div>
  </body>
</html>