<%@ include file="/WEB-INF/views/util.jsp" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title><spring:message code="user.editAccount" text="Editer le profil"/></title>
  </head>

  <body>

    <div class="container">

        <form:form method="POST" modelAttribute="userForm" class="form-signin">
            <h2 class="form-signin-heading"><spring:message code="user.editAccount" text="Editer le profil"/></h2>
            
			<spring:bind path="email">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4>Confirmer le mot de passe</h4>
                    <form:input type="email" path="email" class="form-control"
                                placeholder="Email" value="${userForm.email}"></form:input>
                    <form:errors path="email"></form:errors>
                </div>
            </spring:bind>
            
            <spring:bind path="firstname">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4>Pr�nom</h4>
                    <form:input type="text" path="firstname" class="form-control"
                                placeholder="Pr�nom" value="${userForm.firstname}"></form:input>
                    <form:errors path="firstname"></form:errors>
                </div>
            </spring:bind>
            
            <spring:bind path="lastname">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4>Nom</h4>
                    <form:input type="text" path="lastname" class="form-control"
                                placeholder="Nom" value="${userForm.lastname}"></form:input>
                    <form:errors path="lastname"></form:errors>
                </div>
            </spring:bind>
            
            <spring:bind path="address">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4>Adresse</h4>
                    <form:input type="text" path="address" class="form-control"
                                placeholder="Adresse" value="${userForm.address}"></form:input>
                    <form:errors path="address"></form:errors>
                </div>
            </spring:bind>
            
            <spring:bind path="phone">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                	<h4>T�l�phone</h4>
                    <form:input type="tel" path="phone" class="form-control"
                                placeholder="T�l�phone" value="${userForm.phone}"></form:input>
                    <form:errors path="phone"></form:errors>
                </div>
            </spring:bind>

            <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="button.edit" text="Edit"/></button>
            <span>${message}</span>
        </form:form>
    </div>
  </body>
</html>