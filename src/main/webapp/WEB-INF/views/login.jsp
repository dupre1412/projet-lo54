<%@ include file="/WEB-INF/views/util.jsp" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title><spring:message code="user.loginAccount" text="Connexion au compte"/></title>
  </head>

  <body>

    <div class="container">
      <form name="loginForm" action="<c:url value="/login"/>" method="post"">
        <h2 class="form-heading"><spring:message code="user.loginAccount" text="Connexion au compte"/></h2>

        <div class="form-group ${error != null ? 'has-error' : ''}">
            <span>${message}</span>
            <h4><spring:message code="user.username" text="Identifiant"/></h4>
            <input name="username" type="text" class="form-control" placeholder="<spring:message code="user.username" text="Identifiant"/>"
                   autofocus="true"/>
            <h4><spring:message code="user.password" text="Mot de passe"/></h4>
            <input name="password" type="password" class="form-control" placeholder="<spring:message code="password" text="Mot de passe"/>"/>
            <span>${error}</span>
            

            <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="button.login" text="Se connecter"/></button>
            <h4 class="text-center"><a href="${contextPath}/registration"><spring:message code="user.createAccount" text="Cr�er un compte"/></a></h4>
        </div>
      </form>
    </div>
  </body>
</html>