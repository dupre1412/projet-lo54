<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
<link rel='stylesheet' href='webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<script src="webjars/jquery/3.4.1/jquery.min.js"></script>
<body style="padding-top:65px">
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
  <a class="navbar-brand" href="#">LO54</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<c:url value="/home"/>">Home</a>
      </li>
      <sec:authorize access="isAuthenticated()">
      <li class="nav-item">
        <a class="nav-link" href="<c:url value="/profile"/>">Profil</a>
      </li>
      </sec:authorize>
    </ul>
    <div class="form-inline my-2 my-lg-0">
		<sec:authorize access="!isAuthenticated()">
		<a class="btn btn-outline-light my-2 my-sm-0" role="button" href="<c:url value="/login"/>">Se connecter</a>
		<p>|</p>
		<a class="btn btn-outline-light my-2 my-sm-0" role="button" href="<c:url value="/registration"/>">S'inscrire</a>
		</sec:authorize>
		<sec:authorize access="isAuthenticated()">
			<form id="logoutForm" method="POST" action="<c:url value="/logout"/>"> 
	          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	      </form>
		  <p class="text-light my-2 my-sm-0 mr-2">Connect� en tant que <span class="text-uppercase font-weight-bold">${pageContext.request.userPrincipal.name}</span> | </p>
		  <a class="btn btn-outline-light my-2 my-sm-0" role="button" href="#" onclick="document.forms['logoutForm'].submit()">Se d�connecter</a>
		</sec:authorize>
    </div>
  </div>
</nav>