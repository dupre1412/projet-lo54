package fr.utbm.projetLO54.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.utbm.projetLO54.entity.CourseSession;

@Entity
@Table(name="COURSE")
public class Course {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "code_course")
	private String codeCourse;
	private String title;
	
	//bi-directional many-to-one association to CourseSession
	@OneToMany(mappedBy="course", fetch = FetchType.EAGER)
	private List<CourseSession> courseSessions;
	
	public Course() {}
	
	public Course(String code, String title) {
		this.codeCourse = code;
		this.title = title;
	}
	
	public String getCode_course() {
		return codeCourse;
	}
	public void setCode_course(String id) {
		this.codeCourse = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<CourseSession> getCourseSessions() {
		return this.courseSessions;
	}

	public void setCourseSessions(List<CourseSession> courseSessions) {
		this.courseSessions = courseSessions;
	}

	public CourseSession addCourseSession(CourseSession courseSession) {
		getCourseSessions().add(courseSession);
		courseSession.setCourse(this);

		return courseSession;
	}

	public CourseSession removeCourseSession(CourseSession courseSession) {
		getCourseSessions().remove(courseSession);
		courseSession.setCourse(null);

		return courseSession;
	}
}
