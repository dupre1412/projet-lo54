package fr.utbm.projetLO54.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import fr.utbm.projetLO54.entity.Client;

@Entity
@Table(name="COURSE_SESSION")
public class CourseSession {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_course_session")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCourseSession;
	
	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@Column(name = "max_student")
	private long maxStudent;
	
	//bi-directional many-to-one association to Client
	@ManyToMany(mappedBy="courseSession",
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
                })
	private List<Client> clients;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "code_course")
	private Course course;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id_location")
	private Location location;
	
    @Transient
    private int currentStudentNumber;
    
    @Transient
    private String status;
	
	public CourseSession() {}

	public CourseSession(Date start, Date end, Course course, Location location, long maxS) {
		this.setStartDate(start);
		this.setEndDate(end);
		this.setCourse(course);
		this.setLocation(location);
		this.setMaxStudent(maxS);
	}

	public int getIdCourseSession() {
		return idCourseSession;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getMaxStudent() {
		return maxStudent;
	}

	public void setMaxStudent(long maxStudent) {
		this.maxStudent = maxStudent;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public List<Client> getClients() {
		return this.clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public void addClient(Client client) {
		this.clients.add(client);
		client.getCourseSessions().add(this);
	}

	public void removeClient(Client client) {
		this.clients.remove(client);
		client.getCourseSessions().remove(this);
	}
	
	public int getCurrentStudentNumber() {
		return this.currentStudentNumber;
	}
	
	public void setCurrentStudentNumber(int nb) {
		this.currentStudentNumber=nb;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setStatus(String status) {
		this.status=status;
	}
}
