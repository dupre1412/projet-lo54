package fr.utbm.projetLO54.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.utbm.projetLO54.entity.CourseSession;

@Entity
@Table(name="LOCATION")
public class Location {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_location")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLocation;
	
	private String city;
	
	//bi-directional many-to-one association to CourseSession
	@OneToMany(mappedBy="location", fetch = FetchType.EAGER)
	private List<CourseSession> courseSessions;
	
	public Location() {}
	
	public Location(String city) {
		this.city = city;
	}
	
	public int getId_location() {
		return idLocation;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public List<CourseSession> getCourseSessions() {
		return this.courseSessions;
	}

	public void setCourseSessions(List<CourseSession> courseSessions) {
		this.courseSessions = courseSessions;
	}

	public CourseSession addCourseSession(CourseSession courseSession) {
		getCourseSessions().add(courseSession);
		courseSession.setLocation(this);

		return courseSession;
	}

	public CourseSession removeCourseSession(CourseSession courseSession) {
		getCourseSessions().remove(courseSession);
		courseSession.setLocation(null);

		return courseSession;
	}
	
}
