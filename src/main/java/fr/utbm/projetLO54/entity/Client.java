package fr.utbm.projetLO54.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Entity;

@Entity
@Table(name="Client")
public class Client {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_client")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idClient;
    private String username;
    private String password;
	private String lastname;
	private String firstname;
	private String address;
	private String phone;
	private String email;
	
    @Transient
    private String passwordConfirm;
	
	@ManyToMany(fetch=FetchType.LAZY,
            cascade = {
                    CascadeType.ALL
                })
	private List<CourseSession> courseSession;
	
    @ManyToMany(fetch=FetchType.LAZY)
    private List<Role> roles;
	
	public Client() {}
	
	public Client(String last, String first, String address, String phone, String email, List<CourseSession> cs) {
		this.lastname=last;
		this.firstname=first;
		this.address=address;
		this.phone=phone;
		this.email=email;
		this.courseSession=cs;
	}

	public int getIdClient() {
		return idClient;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<CourseSession> getCourseSessions() {
		return courseSession;
	}

	public void setCourseSessions(List<CourseSession> courseSession) {
		this.courseSession = courseSession;
	}
	
	public void addCourseSession(CourseSession courseSession) {
		this.courseSession.add(courseSession);
		courseSession.getClients().add(this);
	}

	public void removeCourseSession(CourseSession courseSession) {
		this.courseSession.remove(courseSession);
		courseSession.getClients().remove(this);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
	
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    
	public Role addRole(Role role) {
		getRoles().add(role);
		role.addClient(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.removeClient(this);

		return role;
	}
	
}
