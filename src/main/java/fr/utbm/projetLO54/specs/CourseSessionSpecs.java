package fr.utbm.projetLO54.specs;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.criteria.*;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import fr.utbm.projetLO54.entity.Course;
import fr.utbm.projetLO54.entity.CourseSession;
import fr.utbm.projetLO54.entity.CourseSession_;
import fr.utbm.projetLO54.entity.Course_;
import fr.utbm.projetLO54.entity.Location;
import fr.utbm.projetLO54.entity.Location_;

public class CourseSessionSpecs {
    public static Specification<CourseSession> getCourseSessionByCourseName(String title) {
        return (root, query, criteriaBuilder) -> {
            Join<CourseSession, Course> courseJoin = root.join(CourseSession_.course);
            return criteriaBuilder.equal(courseJoin.get(Course_.title), title);
        };
    }
    
    public static Specification<CourseSession> getCourseSessionByCity(String city) {
        return (root, query, criteriaBuilder) -> {
            Join<CourseSession, Location> courseJoin = root.join(CourseSession_.location);
            return criteriaBuilder.equal(courseJoin.get(Location_.city), city);
        };
    }
    
    public static Specification<CourseSession> getCourseSessionByDate(LocalDate date,String operation) {
        return (root, query, criteriaBuilder) -> {
        	if(operation==">")
        		return criteriaBuilder.greaterThanOrEqualTo(root.get(CourseSession_.startDate),Date.valueOf(date));
        	else
        		return criteriaBuilder.lessThanOrEqualTo(root.get(CourseSession_.endDate),Date.valueOf(date));
        };
    }
}
