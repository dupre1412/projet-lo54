package fr.utbm.projetLO54.controller;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.utbm.projetLO54.entity.Client;
import fr.utbm.projetLO54.entity.Course;
import fr.utbm.projetLO54.entity.CourseSession;
import fr.utbm.projetLO54.mail.SendMailInscription;
import fr.utbm.projetLO54.service.ClientService;
import fr.utbm.projetLO54.service.CourseService;
import fr.utbm.projetLO54.service.CourseSessionService;

@Transactional
@Controller
public class SessionController {

	@Autowired
	private	CourseSessionService courseSessionService;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired 
	private ClientService clientService;
	
	@GetMapping("/sessions")
	public String sessions(Model model, @RequestParam(name="course") String codeCourse) {
		
		Course course=courseService.getCourse(codeCourse).get();
		List<CourseSession> sessions = courseSessionService.findByCourseCode(codeCourse);
		long millis=System.currentTimeMillis();  
		java.sql.Date date=new java.sql.Date(millis);  
		
		for(CourseSession session : sessions) {
			CourseSession sessionBis=courseSessionService.getCourseSession(session.getIdCourseSession());
			session.setCurrentStudentNumber(sessionBis.getClients().size());
			if(date.before(session.getStartDate())) {
				session.setStatus("open");
		    	Authentication user = SecurityContextHolder.getContext().getAuthentication();
		    	if (!(user instanceof AnonymousAuthenticationToken)) {
		    		Client client = clientService.getByUsername(user.getName());
			    	for(CourseSession cs : client.getCourseSessions()) {
			    		if(cs.getIdCourseSession()==session.getIdCourseSession()) {
			    			session.setStatus("already");
			    			break;
			    		}
			    	}
		    	}
			}
			else if(date.before(session.getEndDate())) {
				session.setStatus("ongoing");
			}
			else {
				session.setStatus("ended");
			}
		}
		model.addAttribute("sessions",sessions);
		model.addAttribute("course",course);
		
		return "sessions";
	}
	
	@PostMapping("/sessions")
	@Transactional
	public String sessions(	@RequestParam(name="idSession") int idSession, 
							@RequestParam(name="code") String code,
							@RequestParam(name="startDate") String startDate,
							@RequestParam(name="endDate") String endDate,
							@RequestParam(name="location") String location,
							RedirectAttributes redirectAttributes) throws AddressException, MessagingException {
		CourseSession cs = courseSessionService.getCourseSession(idSession);
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Client client = clientService.getByUsername(user.getUsername());
    	clientService.addCourseSession(client,cs);
    	
    	SendMailInscription.generateAndSendEmail(client.getEmail(), code, startDate, endDate, location);
		
		redirectAttributes.addAttribute("course", code); 
		return "sessions";
	}
	
	@PostMapping("/noSessions")
	@Transactional
	public String noSessions(@RequestParam(name="idSession") int idSession, @RequestParam(name="code") String code, RedirectAttributes redirectAttributes) {
		CourseSession cs = courseSessionService.getCourseSession(idSession);
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Client client = clientService.getByUsername(user.getUsername());
    	clientService.removeCourseSession(client,cs);
		
		redirectAttributes.addAttribute("course", code); 
		return "sessions";
	}
}
