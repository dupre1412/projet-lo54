package fr.utbm.projetLO54.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import fr.utbm.projetLO54.entity.Client;
import fr.utbm.projetLO54.service.ClientService;
import fr.utbm.projetLO54.service.SecurityService;
import fr.utbm.projetLO54.validator.UserValidator;

@Controller
public class UserController {
    @Autowired
    private ClientService clientService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new Client());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") Client userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        clientService.saveClient(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/home";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Votre identifiant ou votre mot de passe sont incorrects.");

        if (logout != null)
            model.addAttribute("message", "Vous vous �tes bien d�connect�.");

        return "login";
    }
    
    @GetMapping("/profile")
    public String profile(Model model, String validate) {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(validate != null) {
        	model.addAttribute("message", "Votre profil a bien �t� modifi�");
        }
    	model.addAttribute("userForm", clientService.findByUsername(user.getUsername()));
    	
    	return "profile";
    }
    
    @PostMapping("/profile")
    public String profile(@ModelAttribute("userForm") Client userForm, BindingResult bindingResult) {
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Client client = clientService.findByUsername(user.getUsername());
    	client.setFirstname(userForm.getFirstname());
    	client.setLastname(userForm.getLastname());
    	client.setEmail(userForm.getEmail());
    	client.setAddress(userForm.getAddress());
    	client.setPhone(userForm.getPhone());
    	clientService.saveClient(client);

        return "profile";
    }
}
