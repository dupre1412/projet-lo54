package fr.utbm.projetLO54.controller;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.utbm.projetLO54.entity.CourseSession;
import fr.utbm.projetLO54.entity.Location;
import fr.utbm.projetLO54.service.CourseSessionService;
import fr.utbm.projetLO54.service.LocationService;


@Controller
public class HomeController {
	
	@Autowired
	private CourseSessionService courseSessionService;
	
	@Autowired 
	private LocationService locationService;

	private String title="";
	private String date="";
	private int location = 0;
	@GetMapping(value = "/home")
	public String home(Locale locale, Model model) {
		List<CourseSession> courses = courseSessionService.findByNextSession(this.title,this.date,this.location);
		List<Location> locations = locationService.getLocations();
		
		model.addAttribute("courses",courses);
		model.addAttribute("title",this.title);
		model.addAttribute("date",this.date);
		model.addAttribute("location",this.location);
		model.addAttribute("locations",locations);
		
		
		
		return "home";
	}
	
	@PostMapping(value ="/home")
	public String home(	@RequestParam(name = "title") String title,
						@RequestParam(name = "session_date") String date,
						@RequestParam(name = "location") int location
			) {
		
		this.title=title;
		this.date=date;
		this.location=location;
		
		return "redirect:/home";
	}
	
	@GetMapping(value = "/")
	public String defaut(Locale locale, Model model) {		
		return "redirect:/home";
	}
	
	
}
