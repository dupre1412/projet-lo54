package fr.utbm.projetLO54.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.utbm.projetLO54.entity.Client;
import fr.utbm.projetLO54.entity.CourseSession;
import fr.utbm.projetLO54.repository.ClientRepository;
import fr.utbm.projetLO54.repository.RoleRepository;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public List < Client > getClients() {
        return clientRepository.findAll();
    }

    @Override
    @Transactional
    public void saveClient(Client client) {
    	client.setPassword(bCryptPasswordEncoder.encode(client.getPassword()));
    	client.setRoles(new ArrayList<>(roleRepository.findAll()));
    	clientRepository.save(client);
    }

    @Override
    @Transactional
    public Client getClient(int id){
        Client c = clientRepository.findById(id).get();
        Object o = c.getCourseSessions().size();
    	return c;
    }

    @Override
    @Transactional
    public void deleteClient(int id) {
    	clientRepository.deleteById(id);
    }
    
    @Override
    @Transactional
    public Client findByUsername(String username) {
    	return clientRepository.findByUsername(username);
    }
    
    @Override
    @Transactional
    public Client getByUsername(String username) {
    	return clientRepository.getByUsername(username);
    }
    
    @Override
    @Transactional
    public void addCourseSession(Client c,CourseSession cs) {
    	clientRepository.addCourseSession(c.getIdClient(),cs.getIdCourseSession());
    }
    
    @Override
    @Transactional
    public void removeCourseSession(Client c,CourseSession cs) {
    	clientRepository.removeCourseSession(c.getIdClient(),cs.getIdCourseSession());
    }
}
