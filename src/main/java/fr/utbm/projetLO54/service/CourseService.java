package fr.utbm.projetLO54.service;

import java.util.List;
import java.util.Optional;

import fr.utbm.projetLO54.entity.Course;

public interface CourseService {

    public List < Course > getCourses();

    public void saveCourse(Course course);

    public Optional<Course> getCourse(String codeCourse);

    public void deleteCourse(String codeCourse);
}
