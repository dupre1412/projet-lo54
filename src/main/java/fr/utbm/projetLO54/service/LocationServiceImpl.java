package fr.utbm.projetLO54.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.utbm.projetLO54.entity.Location;
import fr.utbm.projetLO54.repository.LocationRepository;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    @Transactional
    public List < Location > getLocations() {
        return locationRepository.findAll();
    }

    @Override
    @Transactional
    public void saveLocation(Location loc) {
    	locationRepository.save(loc);
    }

    @Override
    @Transactional
    public Optional<Location> getLocation(int id){
        return locationRepository.findById(id);
    }

    @Override
    @Transactional
    public void deleteLocation(int id) {
    	locationRepository.deleteById(id);
    }
}
