package fr.utbm.projetLO54.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.utbm.projetLO54.entity.Course;
import fr.utbm.projetLO54.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    @Transactional
    public List < Course > getCourses() {
        return courseRepository.findAll();
    }

    @Override
    @Transactional
    public void saveCourse(Course course) {
    	courseRepository.save(course);
    }

    @Override
    @Transactional
    public Optional<Course> getCourse(String code){
        return courseRepository.findByCodeCourse(code);
    }

    @Override
    @Transactional
    public void deleteCourse(String code) {
    	courseRepository.deleteById(code);
    }
}
