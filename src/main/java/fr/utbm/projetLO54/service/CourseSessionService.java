package fr.utbm.projetLO54.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import fr.utbm.projetLO54.entity.CourseSession;

public interface CourseSessionService {

    public List < CourseSession > getCourseSessions();

    public void saveCourseSession(CourseSession cs);

    public CourseSession getCourseSession(int id);

    public void deleteCourseSession(int id);
    
    public List<CourseSession> findByLocationCity(String city);
    
    public List<CourseSession> findByCourseTitle(String title);

	public List<CourseSession> findByDate(LocalDate localDate);
	
	public List<CourseSession> findByNextSession(String title, String date, int location);

	List<CourseSession> findByCourseCode(String code);
}
