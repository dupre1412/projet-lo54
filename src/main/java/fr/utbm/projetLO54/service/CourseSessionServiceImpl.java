package fr.utbm.projetLO54.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.utbm.projetLO54.entity.CourseSession;
import fr.utbm.projetLO54.repository.CourseSessionRepository;
import fr.utbm.projetLO54.specs.CourseSessionSpecs;

@Service
@Transactional
public class CourseSessionServiceImpl implements CourseSessionService {

    @Autowired
    private CourseSessionRepository courseSessionRepository;

    @Override
    @Transactional
    public List < CourseSession > getCourseSessions() {
        return courseSessionRepository.findAll();
    }

    @Override
    @Transactional
    public void saveCourseSession(CourseSession cs) {
    	courseSessionRepository.save(cs);
    }

    @Override
    @Transactional
    public CourseSession getCourseSession(int id){
        CourseSession cs = courseSessionRepository.findById(id).get();
        Object o = cs.getClients().size();
        return cs;
    }

    @Override
    @Transactional
    public void deleteCourseSession(int id) {
    	courseSessionRepository.deleteById(id);
    }
    
    @Override
    @Transactional
    public List<CourseSession> findByLocationCity(String city){
    	return courseSessionRepository.findAll(CourseSessionSpecs.getCourseSessionByCity(city));
    }
    
    @Override
    @Transactional
    public List<CourseSession> findByCourseTitle(String title){
		return courseSessionRepository.findAll(CourseSessionSpecs.getCourseSessionByCourseName(title));
    }
    
    @Override
    @Transactional
    public List<CourseSession> findByDate(LocalDate localDate){
		return courseSessionRepository.findAll(Specification.where(CourseSessionSpecs.getCourseSessionByDate(localDate, ">"))
												.and(CourseSessionSpecs.getCourseSessionByDate(localDate, "<")));
    }

	@Override
	@Transactional
	public List<CourseSession> findByNextSession(String title, String date, int location) {
		if(title!="") {
			if(date!="") {
				if(location!=0) courseSessionRepository.findByNextSessionAll(title,date,location);
				else return courseSessionRepository.findByNextSessionTitleDate(title,date);
			}
			else {
				if(location!=0) return courseSessionRepository.findByNextSessionTitleLocation(title,location);
				else return courseSessionRepository.findByNextSessionTitle(title);
			}
		}
		else {
			if(date!="") {
				if(location!=0) return courseSessionRepository.findByNextSessionDateLocation(date,location);
				else return courseSessionRepository.findByNextSessionDate(date);
			}
			else {
				if(location!=0) return courseSessionRepository.findByNextSessionLocation(location);
				else return courseSessionRepository.findCoursesByNextSession();
			}
		}
		
		
		return courseSessionRepository.findCoursesByNextSession();
	}
	
	@Override
	@Transactional
	public List<CourseSession> findByCourseCode(String code){
		return courseSessionRepository.findByCourseCode(code);
	}
}