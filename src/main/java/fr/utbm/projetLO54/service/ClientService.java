package fr.utbm.projetLO54.service;

import java.util.List;

import fr.utbm.projetLO54.entity.Client;
import fr.utbm.projetLO54.entity.CourseSession;

public interface ClientService {

    public List < Client > getClients();

    public void saveClient(Client client);

    public Client getClient(int id);

    public void deleteClient(int id);

	Client findByUsername(String username);

	Client getByUsername(String username);

	void addCourseSession(Client c, CourseSession cs);

	void removeCourseSession(Client c, CourseSession cs);
}
