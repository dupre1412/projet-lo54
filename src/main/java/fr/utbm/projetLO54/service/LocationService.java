package fr.utbm.projetLO54.service;

import java.util.List;
import java.util.Optional;

import fr.utbm.projetLO54.entity.Location;

public interface LocationService {

    public List < Location > getLocations();

    public void saveLocation(Location loc);

    public Optional<Location> getLocation(int id);

    public void deleteLocation(int id);
}