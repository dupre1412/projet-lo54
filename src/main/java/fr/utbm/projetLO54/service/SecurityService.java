package fr.utbm.projetLO54.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
