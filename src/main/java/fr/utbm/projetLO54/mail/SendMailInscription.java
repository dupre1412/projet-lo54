package fr.utbm.projetLO54.mail;


import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
 
public class SendMailInscription {
 
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
 
	public static void generateAndSendEmail(String mail,String codeCourse, String startDate, String endDate, String location) throws AddressException, MessagingException {
 
		final String fromEmail = "maxime.dupre1214@gmail.com"; 
		final String password = "maximeLO54"; 
		final String toEmail = mail;
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); 
		props.put("mail.smtp.port", "587"); 
		props.put("mail.smtp.auth", "true"); 
		props.put("mail.smtp.starttls.enable", "true"); 
		
		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		
		EmailUtil.sendEmail(session, toEmail,"Confirmation de votre inscription à une session de "+codeCourse,
											"Bonjour, ce mail vous est envoyé afin de confirmer votre inscription à la session du cours de "+codeCourse+" qui débutera le "+startDate+" et se terminera le "+endDate+" et qui aura lieu à "+location);
		
	}
}
