package fr.utbm.projetLO54.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.utbm.projetLO54.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
