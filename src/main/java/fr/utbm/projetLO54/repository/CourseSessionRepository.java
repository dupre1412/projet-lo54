package fr.utbm.projetLO54.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.utbm.projetLO54.entity.CourseSession;

@Repository("courseSessionRepository")
public interface CourseSessionRepository extends JpaRepository<CourseSession,Integer>, JpaSpecificationExecutor<CourseSession> {

	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findCoursesByNextSession();
	
	@Query("SELECT cs,l FROM CourseSession cs INNER JOIN cs.course c INNER JOIN cs.location l WHERE c.codeCourse=:code ORDER BY cs.startDate,cs.endDate")
	List <CourseSession> findByCourseCode(@Param("code") String code);

	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE cs.location.idLocation=:location and cs.startDate<=STR_TO_DATE(:date,'%Y-%m-%d') and cs.endDate>=STR_TO_DATE(:date,'%Y-%m-%d') and c.title LIKE CONCAT('%',:title,'%') GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionAll(@Param("title") String title, @Param("date") String date, @Param("location") int location);
	
	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE cs.location.idLocation=:location and c.title LIKE CONCAT('%',:title,'%') GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionTitleLocation(@Param("title") String title, @Param("location") int location);
	
	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE cs.startDate<=STR_TO_DATE(:date,'%Y-%m-%d') and cs.endDate>=STR_TO_DATE(:date,'%Y-%m-%d') and c.title LIKE CONCAT('%',:title,'%') GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionTitleDate(@Param("title") String title, @Param("date") String date);
	
	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE cs.location.idLocation=:location and cs.startDate<=STR_TO_DATE(:date,'%Y-%m-%d') and cs.endDate>=STR_TO_DATE(:date,'%Y-%m-%d') GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionDateLocation(@Param("date") String date, @Param("location") int location);
	
	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE c.title LIKE CONCAT('%',:title,'%') GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionTitle(@Param("title") String title);
	
	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE cs.location.idLocation=:location GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionLocation(@Param("location") int location);
	
	@Query("SELECT cs,c FROM CourseSession cs INNER JOIN cs.course c WHERE cs.startDate<=STR_TO_DATE(:date,'%Y-%m-%d') and cs.endDate>=STR_TO_DATE(:date,'%Y-%m-%d') GROUP BY c.codeCourse HAVING cs.startDate=min(cs.startDate) ORDER BY cs.startDate")
	List <CourseSession> findByNextSessionDate(@Param("date") String date);
}

