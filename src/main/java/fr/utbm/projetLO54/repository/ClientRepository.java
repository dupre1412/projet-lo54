package fr.utbm.projetLO54.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.utbm.projetLO54.entity.Client;

@Repository("clientRepository")
public interface ClientRepository extends JpaRepository<Client,Integer> {
	Client findByUsername(String username);
	
	@Query("Select c FROM Client c LEFT JOIN FETCH c.courseSession cs WHERE c.username=:username")
	Client getByUsername(@Param("username") String username);
	
	@Modifying
	@Query(value="INSERT INTO client_course_session VALUES(:client,:cs)",nativeQuery=true)
	void addCourseSession(@Param("client") int idClient,@Param("cs") int idCourseSession);
	
	@Modifying
	@Query(value="DELETE FROM client_course_session WHERE clients_id_client=:client and courseSession_id_course_session=:cs",nativeQuery=true)
	void removeCourseSession(@Param("client") int idClient,@Param("cs") int idCourseSession);
}
