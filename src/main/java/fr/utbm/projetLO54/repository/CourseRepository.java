package fr.utbm.projetLO54.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.utbm.projetLO54.entity.Course;

@Repository("courseRepository")
public interface CourseRepository extends JpaRepository<Course,Integer> {

	@Query("Select c from Course c where codeCourse=:code")
	Optional<Course> findByCodeCourse(@Param("code") String code);

	@Query("Delete from Course where codeCourse=:code")
	void deleteById(@Param("code") String code);

}
