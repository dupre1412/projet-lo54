package fr.utbm.projetLO54.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.utbm.projetLO54.entity.Location;

@Repository("locationRepository")
public interface LocationRepository extends JpaRepository<Location,Integer> {

}
