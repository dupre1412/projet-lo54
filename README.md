Installation de l'application :

Pr�-requis :
MYSQL 8.0
(Optionnel) MYSQL 8.0 Server install� et configur� dans les variables d'environnement afin d'installer la base de donn�e via le fichier "db.bat"
Java 8

Installation de la base de donn�e : 
Via l'importation du fichier "db.sql" dans un terminal Mysql
OU
Via le fichier "db.bat" en suivant les instructions

Suite � l'installation de la base de donn�e, dans le dossier du projet dans "src/main/resources/application.properties":
Changer les valeurs des variables "jdbc.username" et "jdbc.password" avec vos identifiants MYSQL avec lesquels vous avez install� la base.

Lancement de l'application:

Via le fichier "run.bat" qui va nettoyer le projet, l'installer et le lancer avec Tomcat.
OU
Via Eclipse, en v�rifiant que le compilateur est bien configur� sur Java 8:
Commencez par lancer l'application en tant que "Maven build" avec comme objectif "clean install" afin de t�l�charger les libraries n�cessaires
Puis lancer l'application en tant que "Maven build" avec comme objectif "tomcat7:run-war" afin de lancer l'application

L'application se trouvera � l'adresse : "localhost:9090/projetLO54/"